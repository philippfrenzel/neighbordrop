<?php $this->beginContent('@app/views/layouts/main_blog.php'); ?>
<div id="content">
  <div class="cms">
    <?= $content; ?>
  </div>
</div><!-- container -->
<?php $this->endContent(); ?>
